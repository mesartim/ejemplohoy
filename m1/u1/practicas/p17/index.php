<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<?php include_once "libreria.php"; ?>
	<link rel="stylesheet" type="text/css" href="p17.css">
</head>
<body>
	<div class="wrapper">
		<div class="cabecera">
			<h2>Noticias</h2>
			<p>En esta web vemos las noticias de la base de datos.<br>
			Podemos introducir nuevas noticias</p>
		</div>
		<div class="cuerpo">
			<div class="superior">
				<div class="noticia">
					<div class="titulo"><script type="text/javascript">document.writeln(datos[0].titulo);</script></div>
					<div class="texto"><script type="text/javascript">document.writeln(datos[0].texto);</script></div>
				</div>
				<div class="noticia">
					<div class="titulo"><script type="text/javascript">document.writeln(datos[1].titulo);</script></div>
					<div class="texto"><script type="text/javascript">document.writeln(datos[1].texto);</script></div>
				</div>
				<div class="noticia">
					<div class="titulo"><script type="text/javascript">document.writeln(datos[2].titulo);</script></div>
					<div class="texto"><script type="text/javascript">document.writeln(datos[2].texto);</script></div>
				</div>
				<div class="noticia">
					<div class="titulo"><script type="text/javascript">document.writeln(datos[3].titulo);</script></div>
					<div class="texto"><script type="text/javascript">document.writeln(datos[3].texto);</script></div>
				</div>
			</div>
			<div class="inferior">
				<div class="noticia">
					<div class="titulo"><script type="text/javascript">document.writeln(datos[4].titulo);</script></div>
					<div class="texto"><script type="text/javascript">document.writeln(datos[4].texto);</script></div>
				</div>
				<div class="noticia">
					<div class="titulo"><script type="text/javascript">document.writeln(datos[5].titulo);</script></div>
					<div class="texto"><script type="text/javascript">document.writeln(datos[5].texto);</script></div>
				</div>
				<div class="noticia">
					<div class="titulo"><script type="text/javascript">document.writeln(datos[6].titulo);</script></div>
					<div class="texto"><script type="text/javascript">document.writeln(datos[6].texto);</script></div>
				</div>
				<div class="noticia">
					<div class="titulo"><script type="text/javascript">document.writeln(datos[7].titulo);</script></div>
					<div class="texto"><script type="text/javascript">document.writeln(datos[7].texto);</script></div>
				</div>
			</div>
		</div>
		<div class="listado">
			<div class="titulo2">LISTADO DE NOTICIAS</div>
			<div class="tabla">
				<div class="fila1">
					<div class="celda">#</div>
					<div class="celda">Título</div>
					<div class="celda">Texto</div>
					<div class="celda">Usuario</div>
				</div>
				<div class="fila">
					<div class="celda"><script type="text/javascript">document.writeln(datos[0].id);</script></div>
					<div class="celda"><script type="text/javascript">document.writeln(datos[0].titulo);</script></div>
					<div class="celda"><script type="text/javascript">document.writeln(datos[0].texto);</script></div>
					<div class="celda"><script type="text/javascript">document.writeln(datos[0].usuario);</script></div>
				</div>
				<div class="fila">
					<div class="celda"><script type="text/javascript">document.writeln(datos[1].id);</script></div>
					<div class="celda"><script type="text/javascript">document.writeln(datos[1].titulo);</script></div>
					<div class="celda"><script type="text/javascript">document.writeln(datos[1].texto);</script></div>
					<div class="celda"><script type="text/javascript">document.writeln(datos[1].usuario);</script></div>
				</div>
				<div class="fila">
					<div class="celda"><script type="text/javascript">document.writeln(datos[2].id);</script></div>
					<div class="celda"><script type="text/javascript">document.writeln(datos[2].titulo);</script></div>
					<div class="celda"><script type="text/javascript">document.writeln(datos[2].texto);</script></div>
					<div class="celda"><script type="text/javascript">document.writeln(datos[2].usuario);</script></div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>